import openpyxl
import calendar
import datetime
import sys

#Para lanzar el programa con prints de seguridad lanzar el programa con el parametro -v
if __name__ == "__main__":
    #Al principio del programa cargamos los workbooks de muestra que necesitamos usar en este caso:
    #En primer lugar la hoja de empregados.xlsx con sus datos correspondientes
    #Segundo cargamos la hoja de nominas muestra que sirve como plantilla de todas las hojas de nominas que deben generarse por cada empleado
    wbEmpregado = openpyxl.load_workbook(
        "../myGest/rrhh/empregados.xlsx")
    sheet = wbEmpregado["empregados"]
    wbNomina = openpyxl.load_workbook(
        "../myGest/rrhh/nominaMuestra.xlsx")
    sheet_to_copy = wbNomina["NominaMuestra"]


    #Un dato que debemos generar con python ya que en principio el uso del programa es mensual, es coger del sistema operativo
    #el mes actual junto con las fechas de inicio y fin de cada mes para tramitar cada nomina en el periodo de un mes.

    today = datetime.date.today()
    year = today.year
    month = today.month
    first, last = calendar.monthrange(year, month)


    # En caso de querer cambiar los periodos de facturacion solo hay que cambiar el dia 1 de comienzo y la variable last del dia final
    firstDay = datetime.datetime(year, month, 1)
    lastDay = datetime.datetime(year, month, last)

    #Generamos una lista para guardar el listado de empleados generados con sus datos correspondientes, innecesario para el
    #programa final, pero útil para en caso de querer modificar el programa posteriormente.
    #fila = 0
    listTest = list()


    #Este for cumple multiples funciones que se iran explicando segun vayan ocurriendo, de inicio hay que saber que el for recorre
    #la hoja de empleados leyendo todos los valores correspondientes a cada empleado segun su fila correspondiente.
    for row in sheet.iter_rows(2):

        #Recogemos los valores con las siguientes variables
        testPar =len(sys.argv) > 1 and sys.argv[1] == "-v"
        if testPar:
            print(
                f"Nome: {row[0].value}, Categoria: {row[1].value}, ContaBancaria: {row[2].value}, Incentivos:{row[3].value}")
        nombre = row[0].value
        categoria = row[1].value
        contaBancaria = row[2].value
        incentivos = row[3].value
        #Segun la categoria de cada empleado se coge el sueldo correspondiente y con el sueldo podemos calcular las pagas prorrateadas
        sheetCat = wbEmpregado["taboas Salariais"]
        match categoria:
            case "A1":
                salario_base = sheetCat["B2"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)
            case "A2":
                salario_base = sheetCat["B3"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)
            case "B1":
                salario_base = sheetCat["B4"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)
            case "B2":
                salario_base = sheetCat["B5"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)
            case "C1":
                salario_base = sheetCat["B6"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)
            case "C2":
                salario_base = sheetCat["B7"].value
                prorrateada = (salario_base * 2) / 12
                if testPar:
                    print(prorrateada)

        if testPar:
            print(salario_base)
        #Generamos un objeto empleado y lo añadimos a la lista, esto es para documentar y comprobar resultados, no se usa
        #en este programa
        empregado = (nombre, categoria, salario_base, contaBancaria, incentivos)
        listTest.append(empregado)
        #A continuacion con los datos recogidos de empregados, y los nuevos calculados como la paga prorrateada,
        #procedemos a insertarlo en la plantilla de nominaMuestra, generando una nueva hoja por empleado con los datos
        #de cada empleado
        sheet_to_copy = wbNomina.active
        new_sheet = wbNomina.copy_worksheet(sheet_to_copy)
        new_sheet.title = nombre
        new_sheet['E2'] = nombre
        new_sheet['F5'] = categoria
        new_sheet['F15'] = incentivos
        new_sheet['F16'] = prorrateada
        new_sheet['B38'] = contaBancaria
        new_sheet['C8'] = firstDay
        new_sheet['D8'] = lastDay
        if testPar:
            print(new_sheet)

        #Guardamos los resultados generando una nueva hoja por cada iteracion en el fichero nominas
        wbNomina.save("../myGest/nominas/nominas.xlsx")
        if testPar:
            print(wbNomina)


