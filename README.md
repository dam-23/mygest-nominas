# myGest Nóminas

## Name
Guillermo Pena Trillo
Gitlab: @Selquins
## Description
El proyecto general es myGuest el cual debe desde distintos programas manejar múltiples necesidades de una empresa como gestion de nominas, registro de compras y ventas, y un informe de herencia. Este apartado se encargara solo del apartado de nominas, el cual gestionara todas las nominas del mes en la que se realicen.


## Authors and acknowledgment
- Pablo Rodriguez (@pablorodri001). 
- David García (@zani1200).
- Alex López (@alexbc16).
- Roi Vilariño (@roi34).
- Pablo Costoya (@p-costoya-liceo).
- Pablo Yllanes (@yllanes.pablo).

## License
Open license.

## Languages
Python.

## Project status
ONGOING
Version 0.01 
